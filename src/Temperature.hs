module Temperature (
  Temperature,
  mkTemperatureFromCelsius,
  mkTemperatureFromFahrenheit,
  location,
  celsius,
  fahrenheit,
) where

import Data.Text (Text)
import Optics (Lens', lens, makeLenses, set, view)

data Temperature = Temperature
  { _location :: Text
  , _celsius :: Float
  }
  deriving (Show)

makeLenses ''Temperature

mkTemperatureFromCelsius :: Text -> Float -> Temperature
mkTemperatureFromCelsius loc c = Temperature loc c

mkTemperatureFromFahrenheit :: Text -> Float -> Temperature
mkTemperatureFromFahrenheit loc f = Temperature loc $ fahrenheitToCelsius f

celsiusToFahrenheit :: Float -> Float
celsiusToFahrenheit c = (c * (9 / 5)) + 32

fahrenheitToCelsius :: Float -> Float
fahrenheitToCelsius f = (f - 32) * (5 / 9)

-- | Example of virtual field.
fahrenheit :: Lens' Temperature Float
fahrenheit = lens getter setter
  where
    getter = celsiusToFahrenheit . view celsius
    setter temp f = set celsius (fahrenheitToCelsius f) temp
