# Local Variables:
# mode: makefile
# indent-tabs-mode: nil
# End:
# vim: set ft=make :

set dotenv-load := true

hoogle:
  #!/usr/bin/env bash
  ghc_version=9.4.7
  project_name=hello-hs-optics
  project_version=0
  html_dir=dist-newstyle/build/x86_64-linux/ghc-${ghc_version}/${project_name}-${project_version}/doc/html
  cabal haddock-project --local
  cabal haddock --haddock-hoogle
  cp -v haddocks/*.* ${html_dir}
  hoogle generate -v --local=${html_dir}/${project_name} --local=haddocks --database=${project_name}.hoo
  hoogle server -p 48080 --database=${project_name}.hoo --local
