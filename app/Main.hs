module Main where

import Optics (over, view)

import Temperature (celsius, fahrenheit, mkTemperatureFromCelsius)

import qualified MyLib

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  MyLib.someFunc

  let temp = mkTemperatureFromCelsius "here" 10.0
  print temp
  print $ "celsius: " <> (show $ view celsius temp)
  print $ "fahrenheit: " <> (show $ view fahrenheit temp)
  print $ "add 18 degrees fahrenheit: " <> (show $ over fahrenheit (+ 18) temp)
