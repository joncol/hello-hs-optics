{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}

module MyLib (
  someFunc,
) where

import MyLib.Types

import Data.Map (Map)
import GHC.Generics (Generic)
import Optics
import Optics.Operators.Unsafe ((^?!))

import qualified Data.Map as Map
import Data.Maybe (fromJust)
import Data.Text (Text)

data Point = Point
  { x :: Int
  , y :: Int
  }
  deriving (Generic, Show)

data Thing
  = This
  | That
  | Good Int
  | WTF Int
  deriving (Generic, Show)

data Dummy = Dummy
  { isDumb :: Bool
  , someNumbers :: [Int]
  , somePoint :: Point
  , someThing :: Thing
  , someThings :: [Thing]
  , someMap :: Map Int Int
  }
  deriving (Generic, Show)

-- | Some example dummy function docstring.
someFunc :: IO ()
someFunc = do
  let clock = Clock 16 15
  print clock

  print $ clock ^. #hour
  let time = Time 1 2 3
  print time
  print $ time ^. #second
  print $ view #minute time

  let clock' = clock & #minute %~ (+ 10)
  print clock'

  let d = Dummy True [3, 1, 4, 5, 9, 2, 6, 5] (Point 1 2) This [This, That] Map.empty
  print d
  print $ d ^. #isDumb
  putStrLn $ "y: " <> show (d ^. #somePoint ^. #y)

  -- Transform the point coordinates.
  let d' = d & #somePoint %~ (\(Point x y) -> Point (-x) (-y))
  print d'

  -- Transform the y coordinate. Note that `(%)` is used instead of `(.)`.
  putStrLn $ "over (#somePoint % #y) (+10) d: " <> show (over (#somePoint % #y) (+ 10) d)
  putStrLn $ "d & #somePoint % #y %~ (+10): " <> show (d & #somePoint % #y %~ (+ 10))

  -- Get an element in a list using `ix`.
  let dix = d ^. #someNumbers ^?! ix 2
  putStrLn $ "dix: " <> show dix

  -- Modify an element in a list using `ix`.
  let dix' = d ^. #someNumbers & ix 2 %~ (+ 1)
  putStrLn $ "dix': " <> show dix'

  -- Modify an element in a list using `ix`, keeping original structure.
  let dix'' = d & #someNumbers % ix 2 %~ (+ 1)
  putStrLn $ "dix'': " <> show dix''

  -- Transform the numbers.
  let d''' = d & #someNumbers %~ fmap succ
  print d'''

  -- Extract a list and transform it using `mapped`, note that structure is
  -- lost.
  let d4 = (over mapped (* 2)) (d ^.. #someNumbers % folded)
  putStrLn $ "d4: " <> show d4

  let d5 = (mapped %~ (* 2)) (d ^.. #someNumbers % folded)
  putStrLn $ "d5: " <> show d5

  -- Transform the nested numbers using `mapped`. No structure lost.
  let d6 = (#someNumbers % mapped %~ (* 2)) d
  putStrLn $ "d6: " <> show d6

  let d7 = d & #someNumbers % mapped %~ (* 2)
  putStrLn $ "d7: " <> show d7

  -- Prisms
  putStrLn $ "preview #_This (WTF 42): " <> show (preview #_This (WTF 42))
  putStrLn $ "preview #_WTF (WTF 42): " <> show (preview #_WTF (WTF 42))
  putStrLn $ "(WTF 42) ^? #_WTF: " <> show ((WTF 42) ^? #_WTF)
  putStrLn $ "(WTF 42) ^?! #_WTF: " <> show ((WTF 42) ^?! #_WTF)

  -- Setting the focus of a prism.
  do
    let d = Dummy True [] (Point 1 2) (Good 4444) [] Map.empty
    putStrLn $ "Setting the focus of a prism (1): " <> show (d & #someThing % #_Good .~ 9999)
    putStrLn $ "Setting the focus of a prism (2): " <> show (d & #someThing .~ Good 8888)

  -- Transform all WTFs into something good.
  let d2 = Dummy True [1] (Point 1 2) That [Good 1, This, WTF (-123), That] Map.empty
  putStrLn $ "d2: " <> show d2

  let d2' =
        d2
          & #someThings
            %~ map
              ( \thing -> case preview #_WTF thing of
                  Nothing -> thing
                  Just x -> Good (-x)
              )
  putStrLn $ "d2': " <> show d2'

  -- Update map, inserting a new value, if one doesn't exist for the key.
  let d3 = d2 & #someMap % at 4 %~ Just . maybe 1 (+ 1)
  putStrLn $ "d3: " <> show d3

  let d4 = d3 & #someMap % at 4 %~ Just . maybe 1 (+ 1)
  putStrLn $ "d4: " <> show d4

  -- Changing type using traversal.
  print $ [1 .. 10] & traversed %~ show

  -- Using `unsafeFiltered`.
  print $ [1 .. 10] & traversed % unsafeFiltered (> 5) %~ (* 10)

  -- Using `unsafeFilteredBy`.
  print $
    [Just 1, Just 2, Nothing, Just 4, Just 5]
      & traversed
        % unsafeFilteredBy _Just
        %~ fmap (* 10)

  -- Using `unsafeFilteredBy` to remove `Nothing` values.
  print $
    [Just 1, Just 2, Nothing, Just 4, Just 5]
      ^.. traversed
      % unsafeFilteredBy _Just

  -- Using `unsafeFilteredBy` with tuples.
  print $
    [(1, Just 1), (2, Just 2), (3, Nothing), (4, Just 4), (5, Just 5)]
      ^.. traversed
      % unsafeFilteredBy (_2 % _Just)

  -- Simple removal of `Just`.
  print $
    [(1, Just 1), (2, Just 2), (4, Just 4), (5, Just 5)]
      & traversed % _2 %~ fromJust

  -- Combining the above 2.
  print $
    [(1, Just 1), (2, Just 2), (3, Nothing), (4, Just 4), (5, Just 5)]
      ^.. traversed
      % unsafeFilteredBy (_2 % _Just)
      & traversed % _2 %~ fromJust

  -- `partsOf`
  print $
    [('a', 1), ('b', 2), ('c', 3)]
      & partsOf (traversed % _2) %~ \xs -> map (/ sum xs) xs

  -- `filtered`
  print $ [1 :: Int .. 10] ^.. folded % filtered even

  print $
    [('a', 1 :: Int), ('b', 2), ('c', 3), ('d', 4), ('e', 5), ('f', 6)]
      ^.. folded
      % filtered (even . view _2)

  let xs =
        [ ('a', Just (1 :: Int))
        , ('b', Just 2)
        , ('c', Nothing)
        , ('d', Just 4)
        , ('e', Just 5)
        , ('f', Nothing)
        ]

  print $
    xs
      ^.. folded
      % aside (_Just)
      % filtered (even . view _2)

  print $
    xs
      ^.. folded
      % _2
      % _Just

  print $
    [(1, "hello" :: String), (2, "world"), (3, "foo")] ^.. folded % _2 % to length

  print $
    [(1, "hello" :: String), (2, "world"), (3, "foo")] ^. _2 % to length %& mapping

  print $
    [(1, Left "hello"), (2, Right "world"), (3, Left "foo")] ^.. folded % _2 % _Left

  let ys = [Right "hello", Right "there", Left "foo", Left "bar", Right "world", Left "baz"]
  print $ (ys ^.. folded % _Right) `zip` [0 ..]
  print $
    lookupOf
      folded
      1
      [ (1, "first thing")
      , (2, "second thing")
      , (3, "third thing")
      ]
      ^?! _Just

-- Some prism code based on a question raised by Tomáš Svoboda.
newtype A = A {unA :: Maybe B}
  deriving (Generic)

newtype B = B {unB :: Maybe C}
  deriving (Generic)

newtype C = C {unC :: Maybe Text}
  deriving (Generic)

myfn1 :: C -> Maybe Text
myfn1 c = unC c ^? _Just

myfn2 :: C -> Maybe Text
myfn2 c = c ^. #unC

myfn3 :: Maybe C -> Maybe Text
myfn3 mc = mc ^? (_Just % #unC % _Just)

myfn :: A -> Maybe Text
myfn a = a ^? (#unA % _Just % #unB % _Just % #unC % _Just)

triplets :: [(Text, Text, Maybe Text)]
triplets =
  [ ("hello", "world", Just "foo")
  , ("another", "message", Just "bar")
  , ("shouldn't", "make it through", Nothing)
  , ("something", "else", Just "baz")
  ]

foo :: [(Text, Text, Maybe Text)] -> [(Text, Text, Text)]
foo xs =
  xs
    ^.. traversed
    % unsafeFilteredBy (_3 % _Just)
    & traversed % _3 %~ fromJust

data Person = Person
  { id :: Int
  , name :: Text
  }
  deriving (Show, Generic)

foo2 :: [Person]
foo2 =
  -- xs & traversed %~ (over #name (<> "_YOYO"))
  xs & traversed % unsafeFiltered (\p -> p ^. #id < 10) %~ (over #name (<> "_YOYO"))
  where
    xs :: [Person]
    xs =
      [ Person {id = 1, name = "Foo"}
      , Person {id = 2, name = "Bar"}
      , Person {id = 3, name = "Baz"}
      , Person {id = 11, name = "Hello"}
      , Person {id = 12, name = "World"}
      ]

-- | Update elements of a list at certain indexes.
foo3 :: [Person]
foo3 =
  -- Note that `partsOf` converts a traversal to a lens.
  xs & traversed % partsOf (unsafeFiltered (\p -> p ^. #id `elem` ids)) .~ xs'
  where
    xs :: [Person]
    xs =
      [ Person {id = 1, name = "Foo"}
      , Person {id = 2, name = "Bar"}
      , Person {id = 3, name = "Baz"}
      , Person {id = 11, name = "Hello"}
      , Person {id = 12, name = "World"}
      ]

    ids :: [Int]
    ids = [1, 2, 3]

    xs' :: [Person]
    xs' =
      [ Person {id = 1, name = "!!! Foo"}
      , Person {id = 2, name = "!!! Bar"}
      , Person {id = 3, name = "!!! Baz"}
      , Person {id = 11, name = "!!! Hello"}
      , Person {id = 12, name = "!!! World"}
      ]
