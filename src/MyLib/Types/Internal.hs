{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE UndecidableInstances   #-}

module MyLib.Types.Internal
  where

import Optics.TH

data Clock = Clock { hour   :: Int
                   , minute :: Int
                   }
  deriving (Show)

makeFieldLabelsWith noPrefixFieldLabels ''Clock

data Time = Time { hour   :: Int
                 , minute :: Int
                 , second :: Int
                 }
  deriving (Show)

makeFieldLabelsWith noPrefixFieldLabels ''Time
