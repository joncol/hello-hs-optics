module MyLib.Types
  ( Clock (Clock)
  , Time (Time)
  ) where

import MyLib.Types.Internal
